public class Employee extends User {
    private double salary;
    private double years;


    public Employee (String firstName,String lastName,String email,String mobilePhone,String phone,String password,double salary,double years) {
        super(firstName,lastName,email,mobilePhone,phone,password);
        this.salary=salary;
        this.years=years;
    }
    public Employee (String email,String password,double salary,double years){
        super(email,password);
        this.salary=salary;
        this.years=years;
    }
    public void upSalary() {
        if (years <2) {
            this.salary*=1.05;
        }
        else if (years>=2 & years<=5) {
            this.salary*=1.1;
        }
        else {
            this.salary*=1.15;
        }
    }
    public double getSalary() {
        return this.salary;
    }
}
