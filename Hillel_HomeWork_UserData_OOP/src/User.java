import java.util.MissingFormatArgumentException;

public class User {
    private String firstName;
    private String lastName;
    private String email;
    private String mobilePhone;
    private String phone;
    private String password;


    public User (String firstName,String lastName,String email,String mobilePhone,String phone,String password)
    {

        if (Contains(email, '@')) {
            this.email=email;
        }
        else {
            throw new MissingFormatArgumentException("Email must contains \"@\"");
        }
        if (password.length()>=8 & (password.length()<=16)) {
            this.password=password;
        }
        else {
            throw new IllegalArgumentException("Password should by min 8 max 16 symbols");
        }
        this.firstName=firstName;
        this.lastName=lastName;
        this.mobilePhone=mobilePhone;
        this.phone=phone;

    }

    public User (String email,String password)
    {
        this ("","",email,"","",password);
    }

    public String getEmail () {
        return email;
    }
    public String getPassword (){
        return password;
    }
    private boolean Contains (String string,char symbol){
        for(int i=0;i<string.length();i++) {
            if (string.charAt(i) == symbol)
                return true;
        }
        return  false;
    }
}
