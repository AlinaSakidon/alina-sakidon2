import java.util.Scanner;

 // через switch

public class Main {
    public static void main (String[] args){
        while (true) {
            System.out.println("Введите число 1, 2 или 3: ");
            Scanner inputFigure = new Scanner(System.in);
            int a = inputFigure.nextInt();

            switch (a) {
                case 1:
                    System.out.println("Вы ввели 1");
                    break;
                case 2:
                    System.out.println("Вы ввели 2");
                    break;
                case 3:
                    System.out.println("Вы ввели 3");
                    break;
                default:
                    System.out.println("Вы ввели неподходящий символ");
            }
        }

    }
   }

// через if


/*public class Main {
    public static void main(String[] args) {
        while (true) {
            System.out.println("Введите число 1, 2 или 3: ");
            Scanner inputFigure = new Scanner(System.in);
            int a = inputFigure.nextInt();
            if (a == 1) {
                System.out.println("Вы ввели \"1\"");
            } else if (a == 2) {
                System.out.println("Вы ввели \"2\"");
            } else if (a == 3) {
                System.out.println("Вы ввели \"3\"");
            } else {
                System.out.println("Вы ввели неподходящий символ");
            }
        }
    }
}*/
/*


// 5 4 3 2 1


public class Main {
    public static void main(String[] args) {

        for (int i=5;i>0;i--){

            System.out.print(i + " ");
        }
    }
}*/

// таблица умножения на 3

/*public class Main {
    public static void main(String[] args) {
        int a=3;
        for (int i = 1; i <= 10; i++) {
                System.out.println(a + "x" + i + "=" + (a*i));
            }
        }
}*/

//Найти среднее значение суммы чисел от 1 до 100

/*
public class Main {
    public static void main(String[] args) {
        int num = 100;
        int sum = 0;
        for (int i = 1; i <= num; i++) {
            sum += i;
        }
        int avg = sum / num;
        System.out.println("среднее значение суммы чисел от 1 до 100: " + (double)sum / (double) num);
    }
}
*/
