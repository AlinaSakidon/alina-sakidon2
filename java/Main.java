import org.junit.*;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.nio.file.WatchEvent;
import java.security.Key;
import java.util.List;
import java.util.Locale;

public class Main {
    private WebDriver driver = new ChromeDriver();
    private static String email;
    private static String mobPhone;
    private static String firstName = "Alina";
    private static String lastName = "Sakidon";
    private static String workPhone = "12345";
    private static String password = "123456As";

    @BeforeClass
    public static void setUp() {
        final String path = String.format("%s/bin/chromedriver.exe", System.getProperty("user.dir"));
        System.setProperty("webdriver.chrome.driver", path);

        System.out.println("start tests");
    }

    @Before
    public void Settings() {
        driver.manage().window().maximize();

    }
    @After
    public  void closeBrowsers() {
        driver.quit();
    }

    public String makeRandomEmail() {
        var strValues = "abctropqg12L";
        var strEmail = "";
        for (var i = 0; i < 12; i++) {
            float randomValue = (float) ((strValues.length() - 1) * Math.random());

            strEmail += strValues.charAt(Math.round(randomValue));
        }
        strEmail += "@gmail.com";
        return strEmail;
    }

    public String makeRandomPhone() {
        return "38050" + Math.round(1000000 + Math.random() * 9000000);
    }

    public void clearFields() {//очищение текстовых полей для заполнения юзера с новыми рандом данными
        driver.findElement(By.id("first_name")).clear();
        driver.findElement(By.id("last_name")).clear();
        driver.findElement(By.id("field_work_phone")).clear();
        driver.findElement(By.id("field_phone")).clear();
        driver.findElement(By.id("field_email")).clear();
        driver.findElement(By.id("field_password")).clear();
    }

    @Test
    public void TestRegistration() throws InterruptedException {
        driver.get("http://user-data.hillel.it/html/registration.html");
        driver.findElement(By.className("registration")).click();
        var userIsCreated = false;
        for (int i = 0; i < 3; i++) {//создавать уникального юзера хотябы 3 раза, если данные рандома не уникальны

            email = makeRandomEmail();
            mobPhone = makeRandomPhone();


            driver.findElement(By.id("first_name")).sendKeys(firstName,Keys.ENTER);
            driver.findElement(By.id("last_name")).sendKeys(lastName,Keys.ENTER);
            driver.findElement(By.id("field_work_phone")).sendKeys(workPhone,Keys.ENTER);
            driver.findElement(By.id("field_phone")).sendKeys(mobPhone, Keys.ENTER);
            driver.findElement(By.id("field_email")).sendKeys(email, Keys.ENTER);
            driver.findElement(By.id("field_password")).sendKeys(password, Keys.ENTER);
            driver.findElement(By.id("female")).click();
            driver.findElement(By.id("position")).click();
            Select drpPosition = new Select(driver.findElement(By.name("position")));
            drpPosition.selectByValue("qa");
            driver.findElement(By.id("button_account")).click();
            WebDriverWait wait = new WebDriverWait(driver, 10);
            if (wait.until(ExpectedConditions.alertIsPresent()) != null) {
                var messageText = driver.switchTo().alert().getText();
                if (messageText.contains("Successful registration")) {
                    userIsCreated = true;
                    driver.switchTo().alert().accept();
                    break;
                } else {
                    System.out.println(messageText);
                    driver.switchTo().alert().accept();
                    clearFields();
                    email = null;
                    mobPhone = null;
                }
            }
        }
        if(userIsCreated == false) {
        Assert.fail("after 3 attempts user has not been created");
        }

        System.out.println("Successful registration of user" +" "+ firstName +" " + lastName);
    }

   @Test
    public void testAuthorizationAndCheck() throws InterruptedException {
       if (email == null || mobPhone == null) {//чтобы тест упал, если нет зарегистр юзера
           Assert.fail();
       }
       driver.get("http://user-data.hillel.it/html/registration.html");
       driver.findElement(By.id("email")).sendKeys(email, Keys.ENTER);
       driver.findElement(By.id("password")).sendKeys(password, Keys.ENTER);
       driver.findElement(By.className("login_button")).click();
       WebDriverWait wait = new WebDriverWait(driver, 10);

       wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("employees"))).click();
       System.out.println("The user " + "" + "has been authorizated");
       wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("tab_employees")));
       List<WebElement> td = driver.findElements(By.tagName("td"));
      var emailToLower= email.toLowerCase();

       var userIsFound = false;
       for (int i = 0; i < td.size(); i++) {
           if (td.get(i).getText().toLowerCase().equals(emailToLower)) {
               userIsFound = true;
               break;
           }
       }
       Assert.assertEquals(true, userIsFound);
       //проверить данные юзера в MyInfo вкладке

        driver.findElement(By.id("info")).click();

       wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("work_information")));

        String actualFirstName = driver.findElement(By.id("first_name_myInfo")).getAttribute("value");
        Assert.assertEquals("Wrong firstname",firstName,actualFirstName);
        System.out.println(firstName + " " +actualFirstName);

        String actualLastName = driver.findElement(By.id("last_name_myInfo")).getAttribute("value");
        Assert.assertEquals("Wrong lastName",lastName,actualLastName);
        System.out.println(lastName + " " + actualLastName);

        String actualMail = driver.findElement(By.id("email_MyInfo")).getAttribute("value");
        Assert.assertEquals("Wrong email",emailToLower,actualMail);
        System.out.println(emailToLower + " " + actualMail);

        String actualMobile = driver.findElement(By.id("mobile_phone_my_info")).getAttribute("value");
        Assert.assertEquals("Wrong mob number",mobPhone,actualMobile);
        System.out.println(mobPhone + " " + actualMobile);

        String actualPhone = driver.findElement(By.id("phone_my_info")).getAttribute("value");
        Assert.assertEquals("Wrong phone",workPhone,actualPhone);
        System.out.println(workPhone + " " + actualPhone);

        String actualGender = driver.findElement(By.id("gender_my_info")).getAttribute("value");
        Assert.assertEquals("Wrong gender","female",actualGender);
        System.out.println("female" + " " + actualGender);

        String actualPosition = driver.findElement(By.id("position_my_info")).getAttribute("value");
        Assert.assertEquals("Wrong position","qa",actualPosition);
        System.out.println("qa" + " " + actualPosition);

    }
}



